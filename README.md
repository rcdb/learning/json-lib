# Реализовать парсер данных в формате JSON.

## JSON-текст

Представляет собой в закодированном виде одну из двух структур:

Объект - это неупорядоченное множество пар ключ:значение, разделяемых запятыми. Всё множество заключается в фигурные скобки {}. Ключом может быть только регистрозависимая строка. Значением может быть объект, массив, число, литералы (true, false, null) или строка;
Массив - это упорядоченное множество значений. Массив заключается в квадратные скобки [], а значения разделяются запятыми.
Собственно, динамический полиморфизм во всей красе. Есть, конечно, ограниченный набор типов, но в конкретный момент времени мы можем узнать тип переменной только с помощью RTTI

Пример входных данных в формате JSON:
```JSON
{
    "lastname" : "Ivanov",
    "firstname" : "Ivan",
    "age" : 25,
    "islegal" : false,
    "marks" : [
    	4,5,5,5,2,3
    ],
    "address" : {
    	"city" : "Moscow",
        "street" : "Vozdvijenka"
    }
}
```

Более подробное описание формата можно найти по ссылке json.org


## Задача 1

Разработать библиотеку, которая будет предоставлять следующий интерфейс (пока что c++, попозже на java переделаю)

```c++
class json_value {
public:
    ...

    ~json_value() = default;

    // Правило пяти
    json_value();

    json_value(const json_value &other) = default;

    json_value(json_value &&other) noexcept = default;

    json_value &operator=(const json_value &other) = default;

    json_value &operator=(json_value &&other) noexcept = default;
    // Правило пяти


    // Конструкторы из объектов
    json_value(object_type object);

    json_value(array_type array);

    json_value(string_type string);

    json_value(number_type number);

    json_value(boolean_type boolean);

    json_value(null_type null);
    // Конструкторы из объектов

    
    // Получение типа значения
    [[nodiscard]] json_type get_type() const;

    
    // Получение объектов
    [[nodiscard]] object_type &get_object();

    [[nodiscard]]  array_type &get_array();

    [[nodiscard]]  string_type &get_string();

    [[nodiscard]] number_type get_number();

    [[nodiscard]] boolean_type get_boolean();

    [[nodiscard]] null_type get_null();

    [[nodiscard]] const object_type &get_object() const;

    [[nodiscard]] const array_type &get_array() const;

    [[nodiscard]] const string_type &get_string() const;

    [[nodiscard]] number_type get_number() const;

    [[nodiscard]] boolean_type get_boolean() const;

    [[nodiscard]] null_type get_null() const;
    // Получение объектов

    // Операторы для объектов и массивов
    json_value &operator[](const std::string &key);

    json_value &operator[](int index);

    json_value &operator[](size_t index);

    [[nodiscard]] const json_value &operator[](const std::string &key) const;

    [[nodiscard]] const json_value &operator[](int index) const;

    [[nodiscard]] const json_value &operator[](size_t index) const;
    // Операторы для объектов и массивов
    
    // Статические методы для парсинга и дампа
    static json_value parse_json(std::string json);

    static std::string dump_json(int indent = 0, const json_value &value = json_value());

    static json_value parse_json(const std::filesystem::path &path);
    // Статические методы для парсинга и дампа
    ...
};
```

## Задача 2

На вход программы поступает информация о биржевых инструментах (массив тикеров, массив идентификаторов и массив описаний) в формате JSON вида:

```json
[
    ["Si-9.15", "RTS-9.15", "GAZP-9.15"],
    [100024, 100027, 100050],
    ["Futures contract for USD/RUB", "Futures contract for index RTS", "Futures contract for Gazprom shares"]
]
```
Необходимо выполнить преобразование данных в более удобный для анализа формат:
```json
[
    {"ticker":"Si-9.15", "id": 100024, "description": "Futures contract for USD/RUB"},
    {"ticker":"RTS-9.15", "id": 100027, "description": "Futures contract for index RTS"},
    {"ticker":"GAZP-9.15", "id": 100050, "description": "Futures contract for GAZPROM shares"}
]
```

## Задание 3

В папке ```data``` находится файл ```users.json```. Данный файл представляет собой дамп из базы данных пользователей. Необходимо распределить пользователей по городам и странам

```json
[
    {
    "status": "OK",
    "code": 200,
    "total": 1000,
    "data": [
        {
        "uuid": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11",
        "first_name": "John",
        "last_name": "Doe",
        "birthday": "1990-11-15",
        "city": "Moscow",
        "country": "Russia"
        },
        ...
    ]
    },
    ...
]
```

После распределения внешний вид этого json будет следующий:
```json
[
    "Russia": [
        "Moscow": [
        {
          "uuid": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11",
          "first_name": "John",
          "last_name": "Doe",
          "birthday": "1990-11-15"
        },
        ...  
        ],
        ...
    ],
    ...    
]
```


### Задание 4

Покрыть UNIT-тестами библиотеку. Можно выбрать библиотеки для тестов какие угодно, но важно, чтобы покрытие было хотя бы **97%**